#include <string>

enum ERROR_TYPE{
	NOT_DIR,
	INVALID_PATH
};

class error_data{
	public:
		std::string name;
		std::string msg;
		ERROR_TYPE type;
		void set_val(std::string n, std::string m, ERROR_TYPE t){
			this->name = n;
			this->msg = m;
			this->type = t;
		}
};
class err : public std::exception{
	protected:
		error_data err_data;
	public:
		err(std::string n, std::string m, ERROR_TYPE t){err_data.set_val(n, m, t);}
		std::string type_name(ERROR_TYPE type){
			switch(type){
				case 0:
					return "NOT DIR";
				case 1:
					return "INVALID PATH";
				default:
					return "No type specified";
			}
		}
		std::string name(){
			return this->err_data.name;}
		std::string msg(){
			return this->err_data.msg;}
		ERROR_TYPE type(){
			return this->err_data.type;}
		std::string what(){
			return this->name()+ ": " + this->type_name(this->type())+ " " + this->msg();
		}
};
