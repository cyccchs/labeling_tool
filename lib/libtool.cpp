#include "libtool.h"

//returning the list of directories under the input path
std::vector<path> dir_list(path p){
	if(is_directory(p)){
		std::vector<path> path_vec;
		for(const path& it : directory_iterator(p)){
			if(is_directory(it))
				path_vec.push_back(it);
		}
		sort(path_vec.begin(), path_vec.end());
		return path_vec;
	}
	else
		throw err(__func__, p.string(), NOT_DIR);
}

//returning the list of files under the input path
std::vector<path> file_list(path p){
	if(is_directory(p)){
		std::vector<path> path_vec;
		for(const path& it : directory_iterator(p)){
			if(is_regular_file(it))
				path_vec.push_back(it);
		}
		sort(path_vec.begin(), path_vec.end());
		return path_vec;
	}
	else
		throw err(__func__, p.string(), NOT_DIR);
}

//padding zero to all the files under the input path
//returning the list of renamed files under the input path 
void rename_zero_padding(path p){
	if(is_directory(p)){
		std::vector<path> list = file_list(p);
		for(path it : list){
			std::string tmp;
			for(char itt : it.stem().string()){
				if( isdigit(itt) )
					tmp.push_back(itt);
			}
			std::ostringstream ss;
			ss << std::setw(5) << std::setfill('0') << stoi(tmp);
			rename(it ,it.parent_path().string() + "/" + ss.str() + ".jpg");
		}
	}
	else
		throw err(__func__, p.string(), NOT_DIR);
}

void create_config(path p){
	if(is_directory(p)){
		std::string target_p = p.string() + "/tool.config";
		if(!exists(target_p)){
			std::fstream f; 
			f.open( target_p, std::fstream::out);
			std::cout << target_p << " ----------created" << std::endl;
		}
		else
			std::cout << target_p << " ----------already exsists" << std::endl;
	}
	else
		throw err(__func__, p.string(), NOT_DIR);
}

void load_config(path p){
	if(is_directory(p)){
		std::string target_p = p.string() + "/tool.config";
		if(exists(target_p)){
			std::fstream f;
			f.open(target_p, std::fstream::in);
			std::cout << target_p << " ----------loaded" << std::endl;
		}
		else
			create_config(p);
	}
	else
		throw err(__func__, p.string(), NOT_DIR);
}

void save_config(path p){

}
