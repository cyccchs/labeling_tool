#include <iostream>
#include <filesystem>
#include <fstream>
#include <string>
#include <vector>
#include <opencv2/opencv.hpp>

#include "errorHandler.h"

using namespace std::filesystem;

//returning the list of directories under the input path
std::vector<path> dir_list(path p);

//returning the list of files under the input path
std::vector<path> file_list(path p);

//padding zero to all the files under the input path
//returning the list of renamed files under the input path 
void rename_zero_padding(path p);

void create_config(path p);

void load_config(path p);

void save_config(path p);



