#include "libtool.h"

using namespace std;

int main(void)
{
	try{
		cout << "Source img path " <<  current_path().string() << "/source_img" << endl;
		vector<path> dlist = dir_list(current_path().string() + "/source_img");
		path a("i'm/wrong/path");
		cout << current_path().string() << endl;
		load_config("./");
	}catch(err& e){
		cout << e.what() << endl;
	}catch(...){
		cout << "Unexpected Error" << endl;
	}

	return 0;
}
